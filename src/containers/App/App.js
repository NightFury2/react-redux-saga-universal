import React, { Component, PropTypes } from 'react';
// import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import config from '../../config';
import {getServerInfo} from '../../sagas/serverInfo';

export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  };

  render() {
    const styles = require('./App.scss');

    return (
      <div className={styles.app}>
        <Helmet {...config.app.head}/>

        <div className={styles.appContent}>
          {this.props.children}
        </div>

      </div>
    );
  }
}

function preload() {
  return [
    [getServerInfo]
  ];
}
App.preload = preload;
