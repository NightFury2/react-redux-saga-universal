import React from 'react';
import { render } from 'react-dom';
// import GoogleAnalytics from 'react-ga';

import { browserHistory } from 'react-router';
import { Root } from 'containers';
import rootSaga from './sagas';
import getRoutes from './routes';
import configureStore from './store/configureStore';
import { syncHistoryWithStore} from 'react-router-redux';
import useScroll from 'scroll-behavior/lib/useStandardScroll';

const _browserHistory = useScroll(() => browserHistory)();
const dest = document.getElementById('content');
const store = configureStore(window.__data); // eslint-disable-line
const history = syncHistoryWithStore(_browserHistory, store);

// GoogleAnalytics.initialize(config.app.googleAnalytics.appId);

store.runSaga(rootSaga);

render(
  <Root
    store={store}
    history={history}
    routes={getRoutes(store)}
  />,
  dest
);

if (process.env.NODE_ENV !== 'production') {
  window.React = React; // enable debugger
}
