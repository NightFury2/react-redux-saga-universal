# React Redux Saga Universal 

## Installation

```bash
npm install
```

## Running Dev Server

```bash
npm run dev
```

### Using Redux DevTools

[Redux Devtools](https://github.com/gaearon/redux-devtools) are enabled by default in development.

- <kbd>CTRL</kbd>+<kbd>H</kbd> Toggle DevTools Dock
- <kbd>CTRL</kbd>+<kbd>Q</kbd> Move DevTools Dock Position
- see [redux-devtools-dock-monitor](https://github.com/gaearon/redux-devtools-dock-monitor) for more detailed information.

## Building and Running Production Server

```bash
npm run build
npm run start
```

## Demo

[heroku](https://react-redux.herokuapp.com)

## Contributing

– Lunev Vova, [@NightFury2](https://github.com/NightFury2)
# My project's README
